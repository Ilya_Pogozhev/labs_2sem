import lab1lib


def test_bubble():
    result = lab1lib.bubble_sort([4, 5, 1, 3, 5, 8, 1, 6, 10, 20, 1, 3])
    assert result == [1, 1, 1, 3, 3, 4, 5, 5, 6, 8, 10, 20]


def test_insertion():
    result = lab1lib.insertion_sort([4, 5, 1, 3, 5, 8, 1, 6, 10, 20, 1, 3])
    assert result == [1, 1, 1, 3, 3, 4, 5, 5, 6, 8, 10, 20]


'''def test_shell():
    result = lab1lib.shell([4, 5, 1, 3, 5, 8, 1, 6, 10, 20, 1, 3])
    assert result == [1, 1, 1, 3, 3, 4, 5, 5, 6, 8, 10, 20]


def test_quick():
    result = lab1lib.quick_sort([4, 5, 1, 3, 5, 8, 1, 6, 10, 20, 1, 3])
    assert result == [1, 1, 1, 3, 3, 4, 5, 5, 6, 8, 10, 20]'''

def test_selection_sort():
    result = lab1lib.selection_sort([4, 5, 1, 3, 5, 8, 1, 6, 10, 20, 1, 3])
    assert result == [1, 1, 1, 3, 3, 4, 5, 5, 6, 8, 10, 20]

def test_introsort():
    result = lab1lib.introsort([4, 5, 1, 3, 5, 8, 1, 6, 10, 20, 1, 3])
    assert result == [1, 1, 1, 3, 3, 4, 5, 5, 6, 8, 10, 20]


def test_sustainability():
    array = [(1, 5), (1, 5), (1, 6),  (4, 7), (8, 2), (9, 3), (5, 4)]
    default_sort = sorted(array)
    my_sort = lab1lib.bubble_sort(array)
    my_sort1 = lab1lib.shell(array)
    my_sort2 = lab1lib.insertion_sort(array)
    my_sort3 = lab1lib.quick_sort(array)
    my_sort4 = lab1lib.selection_sort(array)
    my_sort5 = lab1lib.introsort(array)
    assert default_sort == my_sort
    assert default_sort == my_sort1
    assert default_sort == my_sort2
    assert default_sort == my_sort3
    assert default_sort == my_sort4
    assert default_sort == my_sort5

def test_backward():
    result = lab1lib.bubble_sort([5, 7, 1, 10, 8])
    result.reverse()
    assert result == [10, 8, 7, 5, 1]
    result = lab1lib.insertion_sort([5, 7, 1, 10, 8])
    result.reverse()
    assert result == [10, 8, 7, 5, 1]
    result = lab1lib.shell([5, 7, 1, 10, 8])
    result.reverse()
    assert result == [10, 8, 7, 5, 1]
    result = lab1lib.quick_sort([5, 7, 1, 10, 8])
    result.reverse()
    assert result == [10, 8, 7, 5, 1]
    result = lab1lib.selection_sort([5, 7, 1, 10, 8])
    result.reverse()
    assert result == [10, 8, 7, 5, 1]
    result = lab1lib.introsort([5, 7, 1, 10, 8])
    result.reverse()
    assert result == [10, 8, 7, 5, 1]