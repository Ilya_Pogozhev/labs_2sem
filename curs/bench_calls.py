import cProfile
import pstats
from pstats import SortKey
from lab1lib import (bubble_sort, insertion_sort, shell, quick_sort, selection_sort, introsort)
import benchmark_util
import matplotlib.pyplot as plt
from bench_constants import FIRST, LAST, STEP, LEN_ARRAY_1, LEN_ARRAY_2, LST_FORMS
from lab1lib2 import (quadratic_complexity, n_logarithm_n_complexity)
flag = True

fig = plt.figure()
plt.subplots_adjust(top=0.936, bottom=0.07, wspace=0.121, right=0.971, left=0.052)
fig.set_size_inches(11.69, 8.27, forward=True)
for f in LST_FORMS:
    sort = f[0]
    rev = f[1]
    operation_lst = []

    for i in range(FIRST, LAST, STEP):
        lst = benchmark_util.create_lst(i, sort=sort, reverse=rev)
        cProfile.run('shell(lst)', 'stats.log')
        with open('output.txt', 'w') as log_file_stream:
            p = pstats.Stats('stats.log', stream=log_file_stream)
            p.strip_dirs().sort_stats(SortKey.CALLS).print_stats()
        f = open('output.txt')
        line = benchmark_util.correct_lines_c(f)
        f.close()
        operation_lst.append(int(line))
    operation_lst_m = [i*100 for i in operation_lst]


plt.subplot()
plt.title('Сложность алгоритма сортировки\n вставками в худшем случае')
model = benchmark_util.get_graph(LEN_ARRAY_1, operation_lst_m,
                         2, 'Эксперем. сложность(худшая)', '-k', 'ok')
benchmark_util.get_graph(LEN_ARRAY_2, model(LEN_ARRAY_2),
                             2, 'Достр. сложность(худшая)', '-k', '+k')

plt.xlabel('длина массива, шт')
plt.savefig('shell.png')
