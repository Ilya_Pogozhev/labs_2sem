import xlrd
import kr4v_lib
import matplotlib.pyplot as plt


file = xlrd.open_workbook('data.xls', formatting_info=True)
sheet = file.sheet_by_index(0)
data = sheet.col_values(0, start_rowx=2, end_rowx=2502)
data = [float(elem) for elem in data]

print(len(data))

mf = kr4v_lib.median_filter(data, 25)
sma = kr4v_lib.sma(data, 25)
ema = kr4v_lib.ema(data, 25)
size = [i for i in range(len(data))]



plt.figure(figsize=(12, 6))
plt.plot(size, data)
plt.plot(size, sma, 'red')
plt.plot(size, mf, 'green')
plt.plot(size, ema, 'darkblue')

plt.legend()
plt.show()


