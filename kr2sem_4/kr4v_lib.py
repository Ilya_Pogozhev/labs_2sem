from numpy import exp, linspace, convolve
import statistics


def median_filter(data, window_size):
    window = []
    output = []
    for elem in data:
        window.append(elem)
        if len(window) > window_size:
            temp = []
            for i in range(len(window)):
                if i == 0:
                    pass
                else:
                    temp.append(window[i])
            window = temp
        if len(window) == 0:
            output = 0
        else:
            output.append(statistics.median(window))
    return output


def sma(data, window):
    if len(data) < window:
        return None
    return sum(data[-window:]) / float(window)


def ema(values, window):
    weights = exp(linspace(-1., 0., window))
    weights /= weights.sum()
    a = convolve(values, weights, mode='full')[:len(values)]
    a[:window] = a[window]
    return a[0]