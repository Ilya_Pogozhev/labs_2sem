import matplotlib.pyplot as plt
from random import uniform


def monte_carlo(j):
    circle_points = 0
    square_points = 0
    pi = 0
    for i in range(j):
        rand_x = uniform(-1, 1)
        rand_y = uniform(-1, 1)
        origin_dist = rand_x ** 2 + rand_y ** 2
        if origin_dist <= 1:
            circle_points += 1
        square_points += 1
        pi = 4 * circle_points / square_points
    return pi

steps = 90
start = 10000
end = 100000
step = 1000

size = []
pi = []
pi1 = [3.14159265359 for i in range(steps)]

for i in range(start, end, step):
    pi.append(monte_carlo(i))
    size.append(i)

fig, ax = plt.subplots()

ax.plot(size, pi1, 'red')
ax.plot(size, pi, '-o')
plt.show()