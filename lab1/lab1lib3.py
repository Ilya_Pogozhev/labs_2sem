import cProfile
import pstats


# Библа вычислительной сложности


def quadratic_complexity(array):
    """Function for quadratic theoretical complexity"""
    # худший случай всех сортировок
    return [e**2 for e in array]
