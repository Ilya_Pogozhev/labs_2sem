import cProfile
import pstats
import lab2v_lib
from bench_lib import calls_counter, graph_creater
from bench_lib import create_array


first = 4 * 2
last = 4 * 20
step = 4 * 2

array_len = [i for i in range(first, last, step)]
operations = []

functions = ['lab2v_lib.linear_search(arrays[0], -1)',
        'lab2v_lib.binary_search(arrays[0], -1)',
        'lab2v_lib.naive_search(arrays[1], "AD")',
        'lab2v_lib.kmp(arrays[1], "KL")']

for e in functions:
    for i in range(first, last, step):
        arrays = create_array(i)
        cProfile.run(e, 'stats.log')
        with open('output.txt', 'w') as log_file:
            p = pstats.Stats('stats.log', stream=log_file)
            p.strip_dirs().sort_stats(pstats.SortKey.CALLS).print_stats()
        f = open('output.txt')
        line = calls_counter(f)
        f.close()
        operations.append(int(line))
    graph_creater(array_len, operations, 2, 'Количество операций')
    operations = []
