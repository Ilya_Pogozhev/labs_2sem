import networkx as nx
import lib_graph

def test_shortest_path():
    # data loading
    graph_data = lib_graph.graph_loader('data.txt')

    # graph create
    nx_graph = lib_graph.get_nx_graph(graph_data)
    my_graph = lib_graph.get_my_graph(graph_data[0], graph_data[1])

    # get shortest_path
    expected_path = nx.shortest_path(nx_graph, source='A', target='D')
    my_path = min([(len(e), e) for e in lib_graph.find_all_paths(my_graph, 'A', 'D')])[1]

    assert my_path == expected_path
